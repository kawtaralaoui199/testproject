# Recipe Application

This repository contains the source code for the Recipe Application built with Vue.js.

## Getting Started

### Prerequisites

- Docker
- Docker Compose

####build and run the recipe application image 

docker build -t recipe-project:0.0.1 .

cd /docker-compose 

docker-compose up 

### Launch App
- npm install
- npm run dev 
- npm run build 
- npm run test 

### Launch App
- npm install
- npm run dev 
- npm run build 
- npm run test 

#branch

pour soucis de versions , le build ne passe pas sur la branche develop ou y'a la plupart des class test pour cela on a pas merger la branch develop sur le main 