declare module '*.vue' {
    import { DefineComponent } from 'vue';
    const component: DefineComponent<{}, {}, any>;
    export default component;
  }
  
  // Ajoutez cette déclaration pour définir le type de import.meta.env
  interface ImportMeta {
    readonly env: {
      readonly BASE_URL: string;
      // Ajoutez d'autres variables d'environnement ici si nécessaire
    };
  }
  