import { mount } from '@vue/test-utils';
import Signin from '../../components/Signin.vue';

describe('SignIn (Integration Test)', () => {
  it('submits the form when the submit button is clicked', async () => {
    const wrapper = mount(Signin);

    // Set input values
    await wrapper.setData({
      username: 'testuser',
      password: 'testpassword',
    });

    // Trigger form submission
    await wrapper.find('form').trigger('submit.prevent');

    // Wait for Vue's next tick to allow for reactivity
    await wrapper.vm.$nextTick();

    // Assert that the form is submitted
    expect(wrapper.find('.form-submitted').exists()).toBe(true);
    expect(wrapper.find('.username').text()).toBe('Username: testuser');
    expect(wrapper.find('.password').text()).toBe('Password: testpassword');
  });
});
