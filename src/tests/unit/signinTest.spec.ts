import { shallowMount } from '@vue/test-utils';
import Signin from '../../components/Signin.vue';

describe('SignIn', () => {
  let wrapper: any; // Declare wrapper variable to hold the shallowMount instance

  beforeEach(() => {
    wrapper = shallowMount(Signin); // Initialize the wrapper before each test
  });

  it('validates the email correctly', () => {
    const vm: any = wrapper.vm;

    // Test valid email
    expect(vm.isValidEmail('test@example.com')).toBe(true);

    // Test invalid email
    expect(vm.isValidEmail('invalidemail')).toBe(false);
  });

  it('submits the form when submitForm method is called', async () => {
    const vm: any = wrapper.vm;

    // Mocking the PocketBase class and its methods
    const createMock = jest.fn().mockResolvedValue({});
    vm.pb = { collection: jest.fn().mockReturnValue({ create: createMock }) };

    // Set input values
    vm.username = 'testuser';
    vm.password = 'testpassword';

    // Call submitForm method
    await vm.submitForm();

    // Expect formSubmitted to be true after submission
    expect(vm.formSubmitted).toBe(true);
  });
});

