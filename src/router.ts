
import Signin from './components/Signin.vue'
import Login from './components/Login.vue'
import Recipe from './components/Recipe.vue'
import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/recipe', 
      name: 'recipe', 
      component: Recipe
    },
    {
      path: '/signin',
      name: 'signin',
      component: Signin
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
  ]
})

export default router